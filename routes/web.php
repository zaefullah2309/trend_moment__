<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ================ INI FUNCTION UNTUK USER ================ */

Route::get('/', function () {
    return view('index');
});

Route::get('/data_prediksi_user', 'UserController@data_prediksi_user');
Route::get('/loginadmin', 'AdminController@loginadmin');
Route::get('/admin', 'AdminController@admin');

Route::get('/hitung', 'AdminController@hitung');
Route::get('/data_prediksi', 'AdminController@data_prediksi');

Route::get('/data_penyakit', 'AdminController@data_penyakit');
Route::post('/data_penyakit/create', 'AdminController@create');
Route::post('/data_penyakit/update/{id}', 'AdminController@update');
Route::get('/data_penyakit/hapus_penyakit/{id}','AdminController@hapus_penyakit');

Route::get('/hasil_prediksi', 'AdminController@hasil_prediksi');