-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2020 at 09:58 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diagnosa`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gejala`
--

CREATE TABLE `gejala` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_gejala` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_gejala` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gejala`
--

INSERT INTO `gejala` (`id`, `kode_gejala`, `nama_gejala`, `bobot`, `created_at`, `updated_at`) VALUES
(1, 'G1', 'Batuk', '1,0', NULL, NULL),
(2, 'G2', 'Sesak Nafas', '0,0', '2020-07-09 08:50:26', '2020-07-09 08:50:26'),
(3, 'G3', 'Nyeri Dada', '0,0', '2020-07-10 01:24:00', '2020-07-10 01:24:00'),
(4, 'G4', 'Mual dan Muntah', '0,0', '2020-07-10 01:24:33', '2020-07-10 01:24:33'),
(5, 'G5', 'Demam', '0,0', '2020-07-10 01:24:45', '2020-07-10 01:24:45'),
(6, 'G6', 'Nyeri Kepala', '0,0', '2020-07-10 01:25:03', '2020-07-10 01:25:03'),
(7, 'G7', 'Nyeri Ulu Hati', '0,0', '2020-07-10 01:25:35', '2020-07-10 01:25:35'),
(8, 'G8', 'Menggigil', '0,0', '2020-07-10 01:25:51', '2020-07-10 01:25:51'),
(9, 'G9', 'Berkeringat Dingin', '0,0', '2020-07-10 01:26:02', '2020-07-10 01:26:02'),
(10, 'G10', 'Susah Tidur', '0,0', '2020-07-10 01:26:48', '2020-07-10 01:26:48'),
(11, 'G11', 'Pilek', '0,0', '2020-07-10 01:27:02', '2020-07-10 01:27:02'),
(12, 'G12', 'Keluar Sekret / Ingus', '0,0', '2020-07-10 01:27:21', '2020-07-10 01:27:21'),
(13, 'G13', 'Mudah Lelah', '0,0', '2020-07-10 01:27:34', '2020-07-10 01:27:34'),
(14, 'G14', 'Kekakuan Sendi', '0,0', '2020-07-10 01:27:51', '2020-07-10 01:27:51'),
(15, 'G15', 'Lidah Berjamur', '0,0', '2020-07-10 01:28:12', '2020-07-10 01:28:12'),
(16, 'G16', 'Nyeri Pada Hidung', '0,0', '2020-07-10 01:28:32', '2020-07-10 01:28:32'),
(17, 'G2', 'Sesak Nafas.', '0,0', '2020-07-10 22:07:53', '2020-07-10 22:07:53'),
(18, 'G1', 'Batukk', '0,0', '2020-07-10 22:12:46', '2020-07-10 22:12:46'),
(19, 'G1', 'Batukk', '0,0', '2020-07-10 22:12:58', '2020-07-10 22:12:58'),
(20, 'G1', 'Batukk', '0,0', '2020-07-10 22:13:09', '2020-07-10 22:13:09'),
(21, 'G1', 'Batukj', '0,0', '2020-07-10 22:13:32', '2020-07-10 22:13:32'),
(22, 'G1', 'Batukj', '0,0', '2020-07-10 22:14:56', '2020-07-10 22:14:56'),
(23, 'G1', 'Batuk', '0,0', '2020-07-10 22:15:12', '2020-07-10 22:15:12'),
(24, 'G1', 'Batuk', '0,0', '2020-07-10 22:16:23', '2020-07-10 22:16:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_07_09_042319_create_penyakit_table', 1),
(4, '2020_07_09_052145_create_gejala_table', 2),
(5, '2020_07_11_090440_create_pasien_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`id`, `nama`, `jenis_kelamin`, `nomor`, `alamat`, `user`, `pass`, `created_at`, `updated_at`) VALUES
(1, 'gg', 'Laki-laki', 'ds', 'dsd', 'fgd', 'fdf', '2020-07-11 02:36:03', '2020-07-11 02:36:03');

-- --------------------------------------------------------

--
-- Table structure for table `penyakit`
--

CREATE TABLE `penyakit` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_penyakit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_penyakit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `penanganan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `penyakit`
--

INSERT INTO `penyakit` (`id`, `kode_penyakit`, `nama_penyakit`, `deskripsi`, `penanganan`, `created_at`, `updated_at`) VALUES
(1, 'P01', 'Asma', 'Asma adalah suatu keadaan dimana saluran nafas mengalami penyempitan karena hiperaktifitas terhadap rangsangan tertentu, yang menyebabkan  normal peradangan, penyempitan ini bersifat berulang namun reversible, dan diantar episode penyempitan bronkus tersebut terdapat keadaan ventilasi yang lebih (Sylvia A.Price).', 'cuci tangan sebelum makan', NULL, NULL),
(2, 'P02', 'Tuberculosis', 'Tuberculosis adalah penyakit infeksi menular yang disebabkan Mycobacterium tuberculosi yang menyerang paru-paru dan hampir seluruh organ tubuh lainnya. Bakteri ini dapat masuk melalui saluran pernapasan dan saluran pencernaan (GI) dan luka terbuka pada kulit. Tetapi paling banyak melalui inhalasi droplet yang berasal dari orang yang terinfeksi bakteri tersebut (Sylvia A.Price).', 'fgf', NULL, NULL),
(3, 'P03', 'Sinusitis ', 'Sinusitis merupakan suatu proses peradangan pada mukosa atau selaput lendir sinus paranasal. Akibat peradangan ini dapat menyebabkan pembentukan cairan atau kerusakan tulang - tulang diwajah (Efiaty, 2007).', 'xd', '2020-07-09 08:10:43', '2020-07-09 08:10:43'),
(4, 'P04', 'Bronkitis', 'Bronkitis adalah suatu infeksi saluran pernapasan yang menyebabkan inflamasi yang mengenai trakea, bronkus utama dan menengah yang bermanifestasi sebagai batuk, dan biasanya akan membaik tanpa terapi dalam 2 minggu. Bronkitis umumnya disebabkan oleh virus seperti Rhinovirus, RSV, vuris influenza, virus parainfluinza, Adenovirus, virus rubeola, dan Paramyxovirus dan bronkitis karena atau Corybacterium diphtheriae (Rahajoe, 2012). ', 'xd', '2020-07-09 08:15:05', '2020-07-09 08:15:05'),
(5, 'P05', 'Pneumonia ', 'Pneumonia adalah salah satu penyakit peradangan akut parankim paru yang biasanya dari suatu infeksi saluran pernapasan bawah akut (ISNBA) (Syliva A.price). Dengan gejala batuk dan disertai dengan sesak nafas yang disebabkan agen infeksius seperti virus, bakteri, mycoplasma (fungsi), dan aspirasi substansi asing,berupa radang paru-paru yang disertai eksudasi dab konsolidasi dan dapat dilihat melalui gambaran radiologis.', 'nijsoa', '2020-07-09 08:15:50', '2020-07-09 08:15:50'),
(6, 'P06', 'ISPA', 'Infeksi saluran pernapasan akut atau ISPA adalah infeksi yang disebabkan oleh virus yang menyerang hidung, trakea (pipa pernapasan), atau paru-paru. Bisa dikatakan ISPA merupakan infeksi yang mengganggu proses pernafasan seseorang.', 'xdx', '2020-07-09 08:16:38', '2020-07-09 08:16:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gejala`
--
ALTER TABLE `gejala`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penyakit`
--
ALTER TABLE `penyakit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gejala`
--
ALTER TABLE `gejala`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `penyakit`
--
ALTER TABLE `penyakit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
