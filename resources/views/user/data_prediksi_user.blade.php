@extends('layout.home')

@section('content')
<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
    width="50%" margin-top="10px" style="margin-top: 200px;">
    <thead>
        <tr class="headings">
            <th class="column-title">No</th>
            <th class="column-title">Nama Penyakit </th>
            <th class="column-title">Bulan Prediksi </th>
            <th class="column-title">Tahun Prediksi </th>
            <th class="column-title">Hasil Prediksi </th>
        </tr>
    </thead>
    <tbody>
        <tr class="odd pointer">
            <td class=" ">1</td>
            <td class=" ">May 23, 2014 11:30:12 PM</td>
            <td class=" ">121000208</td>
            <td class=" ">John Blank L</td>
            <td class=" ">John Blank L</td>
        </tr>
    </tbody>
</table>

@endsection
