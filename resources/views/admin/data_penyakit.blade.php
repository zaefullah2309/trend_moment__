@extends('layout.homeadmin')

@section('content')
<div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>DATA PENYAKIT</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                        data-target="#exampleModal">
                        <i class="fa fa-plus"></i>
                        tambah data
                    </button>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            @if(session('edit'))
            <div class="alert alert-warning" role="alert">
                {{session('edit')}}
            </div>
            @endif
            @if(session('hapus'))
            <div class="alert alert-danger" role="alert">
                {{session('hapus')}}
            </div>
            @endif
            @if(session('sukses'))
            <div class="alert alert-success" role="alert">
                {{session('sukses')}}
            </div>
            @endif
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable-responsive"
                                class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Penyakitt</th>
                                        <th>Bulan</th>
                                        <th>Tahun</th>
                                        <th>Jumlah Penderita</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php($no=1)
                                    @foreach($data as $penyakit)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$penyakit->nama_penyakit}}</td>
                                        <td>{{$penyakit->bulan}}</td>
                                        <td>{{$penyakit->tahun}}</td>
                                        <td>{{$penyakit->jumlah}}</td>
                                        <td>
                                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                                data-target="#exampleModal1{{$penyakit->id}}"><i class="fa fa-pencil"></i>
                                                Edit
                                            </button>
                                            <div class="modal fade" id="exampleModal1{{$penyakit->id}}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Edit Data
                                                                Penyakit</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="/data_penyakit/update/{{$penyakit->id}}" method="POST">
                                                                {{csrf_field()}}
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Nama
                                                                        Penyakit</label>
                                                                    <input name="nama_penyakit" type="text"
                                                                        class="form-control" id="exampleInputEmail1"
                                                                        aria-describedby="emailHelp" value="{{$penyakit->nama_penyakit}}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Bulan</label>
                                                                    <input name="bulan" type="text" class="form-control"
                                                                        id="exampleInputEmail1"
                                                                        aria-describedby="emailHelp" value="{{$penyakit->bulan}}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Tahun</label>
                                                                    <input name="tahun" type="text" class="form-control"
                                                                        id="exampleInputEmail1"
                                                                        aria-describedby="emailHelp" value="{{$penyakit->tahun}}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Jumlah
                                                                        Penderita</label>
                                                                    <input name="jumlah" type="text"
                                                                        class="form-control" id="exampleInputEmail1"
                                                                        aria-describedby="emailHelp" value="{{$penyakit->jumlah}}">
                                                                </div>
                                                                <br>
                                                                <div class="form-group">
                                                                    <button type="button" class="btn btn-danger btn-sm"
                                                                        data-dismiss="modal">Close</button>
                                                                    <button type="submit"
                                                                        class="btn btn-primary btn-sm">Save</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="data_penyakit/hapus_penyakit/{{$penyakit->id}}"
                                                class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i>
                                                Delete </a>
                                        </td>
                                    </tr>
                                    @php($no++)
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Penyakit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/data_penyakit/create" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Penyakit</label>
                        <input name="nama_penyakit" type="text" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Bulan</label>
                        <input name="bulan" type="text" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tahun</label>
                        <input name="tahun" type="text" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Jumlah Penderita</label>
                        <input name="jumlah" type="text" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp">
                    </div>
                    <br>
                    <div class="form-group">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
