@extends('layout.homeadmin')

@section('content')
<div class="right_col" role="main">
		<br>
							<div class="x_panel">                            
								<div class="x_title">
									<h2> PREDIKSI UNTUK TAHUN 2021</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<br />
									<form class="form-horizontal form-label-center" action="/hasil_prediksi" >
										<div class="form-group row">
											<label class="control-label col-md-3 col-sm-3 ">Bulan Yang Akan di Prediksi</label>
											<div class="col-md-9 col-sm-9 ">
												<select class="select2_single form-control" tabindex="-1">
													<option></option>
													<option value="0">Januari</option>
													<option value="1">Februari</option>
													<option value="2">Maret</option>
                                                    <option value="3">April</option>
													<option value="4">Mei</option>
													<option value="5">Juni</option>	
                                                    <option value="6">Juli</option>
													<option value="7">Agustus</option>
													<option value="8">September</option>
                                                    <option value="9">Oktober</option>
													<option value="10">November</option>
													<option value="11">Desember</option>												
												</select>
											</div>
										</div>
										<div class="ln_solid"></div>
										<div class="form-group">
											<div class="col-md-9 col-sm-9  offset-md-3">
												<button type="submit" class="btn btn-success btn-sm">Hitung</button>
											</div>
										</div>

									</form>
								</div>
							</div>
						</div>
    </div>
</div>



@endsection
