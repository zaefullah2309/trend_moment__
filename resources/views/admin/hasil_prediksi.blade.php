@extends('layout.homeadmin')

@section('content')
<div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h3>HASIL PREDIKSI</h3>
            </div>
            <div >
                <h2>Hasil Hitung X, Y, XY, XX, Rata X, dan Rata Y</h2>
            </div>
            <div class="x_content">
                            <table
                                class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th>Bulan (X)</th>
                                        <th>Jumlah Penderita (Y)</th>
                                        <th>Bulan(X) * Jumlah Penderita(Y) </th>
                                        <th>Bulan(X) * Bulan(X)</th>
                                        <th>Rata-rata Bulan(X)</th>
                                        <th>Rata-rata Jumlah Penderita(Y)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        
            </div>
            
            <div >
                <h2>Nilai A dan Nilai B</h2>
            </div>
            <div class="x_content">
                            <table
                                class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th>Nilai A</th>
                                        <th>Nilai B</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        
            </div>

            <div >
                <h2>Hasil Prediksi Bulan Selanjutnya dengan Trend Moment</h2>
            </div>
            <div class="x_content">
                            <table
                                class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th>Bulan </th>
                                        <th>Tahun </th>
                                        <th>Jumlah Penderita</th>
                                        <th>Hasil Perhitungan</th>
                                        <th>Indeks Musim</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        
            </div>
        </div>
    </div>
</div>



@endsection
