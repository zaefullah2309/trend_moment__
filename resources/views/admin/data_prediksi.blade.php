@extends('layout.homeadmin')

@section('content')
<div class="right_col" role="main">
<div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Prediksi</h2>
                    <ul class="nav navbar-right panel_toolbox">                   
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
					
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                          <tr class="headings">
                            <th class="column-title">No</th>
                            <th class="column-title">Nama Penyakit </th>
                            <th class="column-title">Bulan Prediksi </th>
                            <th class="column-title">Tahun Prediksi </th>
                            <th class="column-title">Hasil Prediksi </th>
                            <th class="column-title">Tindakan </th>
                          </tr>                          
                        </thead>
                      <tbody>
                      <tr class="odd pointer">
                            <td class=" ">1</td>
                            <td class=" ">May 23, 2014 11:30:12 PM</td>
                            <td class=" ">121000208</td>
                            <td class=" ">John Blank L</td>
                            <td class=" ">John Blank L</td>
                            <td>
                              <a href="3" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i>
                              Delete </a>
                            </td>
                          </tr>
                      </tbody>
                    </table>							
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</div>

@endsection