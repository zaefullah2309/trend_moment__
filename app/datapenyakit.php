<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class datapenyakit extends Model
{
    protected $table = 'datapenyakit';
    protected $fillable = ['nama_penyakit','bulan','tahun','jumlah'];
}
