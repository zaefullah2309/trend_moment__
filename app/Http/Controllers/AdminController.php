<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\datapenyakit;

class AdminController extends Controller
{

    public function data_penyakit()
    {
        $data = \App\datapenyakit::all();
        return view('admin.data_penyakit', ['data' => $data]);
    }

    public function create(Request $request)
    {
        \App\datapenyakit::create($request->all());
        return redirect ('data_penyakit')->with('sukses','Data Berhasil di Tambahkan');
    }

    public function update(Request $request, $id)
    {
        $penyakit=datapenyakit::find($id);
            $penyakit->nama_penyakit = $request -> input ('nama_penyakit');
            $penyakit->bulan = $request -> input ('bulan');
            $penyakit->tahun = $request -> input ('tahun');
            $penyakit->jumlah = $request -> input ('jumlah');
            $penyakit->save();
        return redirect()->back()->with('edit', 'Data Berhasil di Update');
    }

    public function hapus_penyakit($id)
    {
        $penyakit=datapenyakit::find($id);
            $penyakit->delete();
            return redirect()->back()->with('hapus','Data Berhasil di Hapus');
    }

    public function hitung()
    {
        return view('admin.hitung');
    }
    public function data_prediksi()
    {
        return view('admin.data_prediksi');
    }
    public function admin()
    {
        return view('admin.admin');
    }
    public function loginadmin()
    {
        return view('admin.loginadmin');
    }

    public function hasil_prediksi()
    {
        return view('admin.hasil_prediksi');
    }

}
